# scq

Command-line utility and a go library for calculating the *"source-comment quality"* of given source code. Can process source code from stdin, from files, or from files inside archives just like [sloc](http://cloc.sourceforge.net/).

Returns the following metrics:  

* % of implementation details commented.
* % of implementation definitions commented.
* % of non-implementation-specific commented.


### Supported Languages

* Go

*Read the documentation to add another supported language.*


## The Catch

Nothing has been written yet.  
I'll begin working on this project at some point in the future. I put this project idea online to see if others get interested in it.

Originally my idea was to analyze GitHub projects and produce a nice graph of commented-code-lines vs uncommented ones per-project and per-user. This may yet happen, and I plan on having this online behind an HTTP API on something like [Heroku](https://heroku.com/) or [OpenShift](https://www.openshift.com/). I do not yet know if this requires cloning or downloading source code from GitHub projects, or whether there is already some other way to get to the source code, but this is a challenge for the future.
